import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Input from '@/components/Input.vue'

describe('Input.vue', () => {
  it('renders props.msg when passed', () => {
    const value = 'new message'
    const wrapper = shallowMount(Input, {
      propsData: {
        value
      }
    })
    expect(wrapper.find('input').props('value')).to.eq(value)
  })
})
