var http = require('http')
const querystring = require('querystring')
http.createServer(function (req, response) {
  if (req.method.toLowerCase() === 'post') {
    var alldata = ''
    req.addListener('data', function (chunk) {
      alldata += chunk
    })
    req.addListener('end', function () {
      var alldatas = querystring.parse(alldata)
      console.log(alldatas)
    })
  }
  response.end('Hello World\n')
}).listen(8888)

// 终端打印如下信息
console.log('Server running at http://127.0.0.1:8888/')
