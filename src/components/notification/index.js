
import Notification from './notification.vue'
import Vue from 'vue'
Notification.newInstance = function (props) {
  props = props || {}
  let instance = new Vue({
    render: function (h) {
      return h(Notification, {
        props
      })
    }
  })

  let component = instance.$mount()
  document.body.appendChild(component.$el)

  const notification = component.$children[0]

  console.log(notification)

  return {
    add (options) {
      notification.add(options)
    },
    close (name) {
      if (name) {
        notification.remove(name)
      } else {
        notification.removeAll()
      }
    }
  }
}

export default Notification
