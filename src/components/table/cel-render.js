export default {
  functional: true,
  props: {
    row: Object,
    col: Object,
    index: Number,
    render: Function
  },
  render: (h, ctx) => {
    const params = {
      row: ctx.props.row,
      col: ctx.props.col,
      index: ctx.props.index
    }
    return ctx.props.render(h, params)
  }
}
