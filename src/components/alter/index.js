import notification from '../notification/'
const prefixCls = 'alter'
let mount = 1
const duration = 2000
let noticeInstance

const getInstance = function () {
  return noticeInstance || notification.newInstance()
}

function alter (type, options) {
  noticeInstance = getInstance()
  console.log(options)
  noticeInstance.add({
    type: type,
    prefixCls: prefixCls,
    name: prefixCls + mount,
    duration,
    ...options
  })
  mount++
}

export default {
  open (options) {
    let type = options.type || 'normal'
    alter(type, options)
  },
  close (name) {
    console.log('noticeInstance', noticeInstance)
    if (noticeInstance) {
      noticeInstance.close(name)
    }
  }
}
