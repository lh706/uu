import Vue from 'vue'
import Router from 'vue-router'
const _import = path => () => import('@/views/' + path)

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/form',
      name: 'form',
      component: _import('form')
    },
    {
      path: '/checkbox',
      name: 'checkbox',
      component: _import('checkbox')
    },
    {
      path: '/alter',
      name: 'alter',
      component: _import('alter')
    },
    {
      path: '/demo',
      name: 'demo',
      component: _import('demo')
    },
    {
      path: '/table',
      name: 'table',
      component: _import('table')
    },
    {
      path: '/tree',
      name: 'tree',
      component: _import('tree')
    }
  ]
})
