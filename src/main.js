import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import alter from './components/alter/'

Vue.config.productionTip = false

Vue.prototype.$Alter = alter

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
